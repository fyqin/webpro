from selenium import webdriver
import pytest
import allure
import os
import time

@allure.feature('电商项目登录测试')
class TestLogin:
    def setup_class(self):
        self.driver = webdriver.Chrome()
        #隐式等待
        self.driver.implicitly_wait(10)

    @allure.story('账号不存在')
    @allure.description('测试账号不存在的用例情况')
    @allure.title('账号不存在')
    def test_no_account(self):
        #账号不存在
        with allure.step('打开登录页面'):
            self.driver.get('http://testingedu.com.cn:8000/home/User/login.html')
        with allure.step('输入用户名'):
            #输入用户名
            self.driver.find_element_by_xpath('//*[@id="username"]').send_keys('11fghfg11')
        #输入密码
        self.driver.find_element_by_xpath('//*[@id="password"]').send_keys('1fdhrt11')
        #输入验证码
        self.driver.find_element_by_xpath('//*[@id="verify_code"]').send_keys('aaaa')
        #点击登录
        self.driver.find_element_by_xpath('//*[@id="loginform"]/div/div[6]/a').click()
        #校验
        text = self.driver.find_element_by_xpath('//*[@id="layui-layer1"]/div[2]').text
        print(text)
        allure.attach(self.driver.get_screenshot_as_png(), '运行截图', attachment_type=allure.attachment_type.PNG)

    @allure.story('账号密码错误')
    def test_error_pwd(self):
        self.driver.get('http://testingedu.com.cn:8000/home/User/login.html')
        # 输入用户名
        self.driver.find_element_by_xpath('//*[@id="username"]').send_keys('13800138006')
        # 输入密码
        self.driver.find_element_by_xpath('//*[@id="password"]').send_keys('153453')
        # 输入验证码
        self.driver.find_element_by_xpath('//*[@id="verify_code"]').send_keys('aaaa')
        # 点击登录
        self.driver.find_element_by_xpath('//*[@id="loginform"]/div/div[6]/a').click()
        # 校验
        text = self.driver.find_element_by_xpath('//*[@id="layui-layer1"]/div[2]').text
        print(text)

    @allure.story('成功登录')
    def test_success_pwd(self):
        self.driver.get('http://testingedu.com.cn:8000/home/User/login.html')
        # 输入用户名
        self.driver.find_element_by_xpath('//*[@id="username"]').send_keys('13800138006')
        # 输入密码
        self.driver.find_element_by_xpath('//*[@id="password"]').send_keys('123456')
        # 输入验证码
        self.driver.find_element_by_xpath('//*[@id="verify_code"]').send_keys('aaaa')
        # 点击登录
        self.driver.find_element_by_xpath('//*[@id="loginform"]/div/div[6]/a').click()
        time.sleep(2)
        # 校验
        text = self.driver.find_element_by_xpath('/html/body/div[1]/div/div/div/div[2]/a[1]').text
        print(text)

    def teardown_class(self):
        self.driver.quit()

if __name__ == "__main__":
    #使用pytest执行用例，并获取allure执行结果
    pytest.main(['-s', './test_Com.py', '--alluredir', 'temp'])
    os.system('allure generate ./temp -o ./report --clean')